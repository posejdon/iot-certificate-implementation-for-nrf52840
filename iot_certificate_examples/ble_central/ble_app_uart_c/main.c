/**
 * Copyright (c) 2016 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "nordic_common.h"
#include "app_error.h"
#include "app_uart.h"
#include "ble_db_discovery.h"
#include "app_timer.h"
#include "app_util.h"
#include "bsp_btn_ble.h"
#include "ble.h"
#include "ble_gap.h"
#include "ble_hci.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "ble_nus_c.h"
#include "nrf_ble_gatt.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_ble_scan.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#include <iot_certificate.h>
#include <iot_utils/iot_certificate_utils.h>
#include <iot_utils/iot_openssl.h>

#define APP_BLE_CONN_CFG_TAG    1                                       /**< Tag that refers to the BLE stack configuration set with @ref sd_ble_cfg_set. The default tag is @ref BLE_CONN_CFG_TAG_DEFAULT. */
#define APP_BLE_OBSERVER_PRIO   3                                       /**< BLE observer priority of the application. There is no need to modify this value. */

#define UART_TX_BUF_SIZE        256                                     /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE        256                                     /**< UART RX buffer size. */

#define NUS_SERVICE_UUID_TYPE   BLE_UUID_TYPE_VENDOR_BEGIN              /**< UUID type for the Nordic UART Service (vendor specific). */

#define ECHOBACK_BLE_UART_DATA  0                                       /**< Echo the UART data that is received over the Nordic UART Service (NUS) back to the sender. */


BLE_NUS_C_DEF(m_ble_nus_c);                                             /**< BLE Nordic UART Service (NUS) client instance. */
NRF_BLE_GATT_DEF(m_gatt);                                               /**< GATT module instance. */
BLE_DB_DISCOVERY_DEF(m_db_disc);                                        /**< Database discovery module instance. */
NRF_BLE_SCAN_DEF(m_scan);                                               /**< Scanning Module instance. */

static uint16_t m_ble_nus_max_data_len = BLE_GATT_ATT_MTU_DEFAULT - OPCODE_LENGTH - HANDLE_LENGTH; /**< Maximum length of data (in bytes) that can be transmitted to the peer by the Nordic UART service module. */

/**@brief NUS UUID. */
static ble_uuid_t const m_nus_uuid =
{
    .uuid = BLE_UUID_NUS_SERVICE,
    .type = NUS_SERVICE_UUID_TYPE
};

static uint8_t PEER_NAME[] = "Nordic_BOB_UART";


/**@brief Function for handling asserts in the SoftDevice.
 *
 * @details This function is called in case of an assert in the SoftDevice.
 *
 * @warning This handler is only an example and is not meant for the final product. You need to analyze
 *          how your product is supposed to react in case of assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num     Line number of the failing assert call.
 * @param[in] p_file_name  File name of the failing assert call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(0xDEADBEEF, line_num, p_file_name);
}


/**@brief Function for starting scanning. */
static void scan_start(void)
{
    ret_code_t ret;

    ret = nrf_ble_scan_start(&m_scan);
    APP_ERROR_CHECK(ret);

    ret = bsp_indication_set(BSP_INDICATE_SCANNING);
    APP_ERROR_CHECK(ret);
}


/**@brief Function for handling Scanning Module events.
 */
static void scan_evt_handler(scan_evt_t const * p_scan_evt)
{
    ret_code_t err_code;

    switch(p_scan_evt->scan_evt_id)
    {
         case NRF_BLE_SCAN_EVT_CONNECTING_ERROR:
         {
              err_code = p_scan_evt->params.connecting_err.err_code;
              APP_ERROR_CHECK(err_code);
         } break;

         case NRF_BLE_SCAN_EVT_CONNECTED:
         {
              ble_gap_evt_connected_t const * p_connected =
                               p_scan_evt->params.connected.p_connected;
             // Scan is automatically stopped by the connection.
             NRF_LOG_INFO("Connecting to target %02x%02x%02x%02x%02x%02x",
                      p_connected->peer_addr.addr[0],
                      p_connected->peer_addr.addr[1],
                      p_connected->peer_addr.addr[2],
                      p_connected->peer_addr.addr[3],
                      p_connected->peer_addr.addr[4],
                      p_connected->peer_addr.addr[5]
                      );
         } break;

         case NRF_BLE_SCAN_EVT_SCAN_TIMEOUT:
         {
             NRF_LOG_INFO("Scan timed out.");
             scan_start();
         } break;

         default:
             break;
    }
}


/**@brief Function for initializing the scanning and setting the filters.
 */
static void scan_init(void)
{
    ret_code_t          err_code;
    nrf_ble_scan_init_t init_scan;

    memset(&init_scan, 0, sizeof(init_scan));

    init_scan.connect_if_match = true;
    init_scan.conn_cfg_tag     = APP_BLE_CONN_CFG_TAG;

    err_code = nrf_ble_scan_init(&m_scan, &init_scan, scan_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_UUID_FILTER, &m_nus_uuid);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_scan_filter_set(&m_scan, SCAN_NAME_FILTER, PEER_NAME);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_scan_filters_enable(&m_scan, NRF_BLE_SCAN_UUID_FILTER | NRF_BLE_SCAN_NAME_FILTER, false);
//    err_code = nrf_ble_scan_filters_enable(&m_scan, NRF_BLE_SCAN_NAME_FILTER, false);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling database discovery events.
 *
 * @details This function is a callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function forwards the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void db_disc_handler(ble_db_discovery_evt_t * p_evt)
{
    ble_nus_c_on_db_disc_evt(&m_ble_nus_c, p_evt);
}

static uint8_t cert_bin[] = {
    0x01, 0x72, 0x6a, 0x30, 0x46, 0x0f, 0x1a, 0x41, 0xa3, 0xa8, 0x4a, 0x82,
    0xf9, 0xbc, 0x09, 0xce, 0x69, 0x5b, 0x00, 0x30, 0x59, 0x30, 0x13, 0x06,
    0x07, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x02, 0x01, 0x06, 0x08, 0x2a, 0x86,
    0x48, 0xce, 0x3d, 0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04, 0x4e, 0x85,
    0x38, 0x35, 0xc7, 0xc1, 0xa4, 0x89, 0x77, 0xb7, 0xd2, 0x83, 0x3c, 0x9a,
    0xa6, 0xb9, 0x07, 0x0c, 0x59, 0x7b, 0x9d, 0xe2, 0xdd, 0xde, 0x87, 0x1b,
    0xd2, 0x41, 0xa1, 0xed, 0xe5, 0x53, 0x40, 0xd6, 0xfe, 0x5c, 0x91, 0x1e,
    0x94, 0x8d, 0xa0, 0x28, 0xaa, 0x38, 0x66, 0x28, 0x01, 0x40, 0xb7, 0xc7,
    0x8d, 0xf0, 0x1a, 0xb8, 0x62, 0x55, 0xa0, 0xa1, 0x8a, 0xea, 0x02, 0x9a,
    0x0e, 0xe5, 0x01, 0x3a, 0x46, 0x6f, 0x75, 0x72, 0x74, 0x68, 0x20, 0x68,
    0x65, 0x61, 0x74, 0x20, 0x64, 0x65, 0x74, 0x65, 0x63, 0x74, 0x6f, 0x72,
    0x20, 0x6d, 0x65, 0x61, 0x73, 0x75, 0x72, 0x69, 0x6e, 0x67, 0x20, 0x74,
    0x65, 0x6d, 0x70, 0x65, 0x72, 0x61, 0x74, 0x75, 0x72, 0x65, 0x20, 0x69,
    0x6e, 0x20, 0x72, 0x6f, 0x6f, 0x6d, 0x20, 0x6e, 0x6f, 0x2e, 0x20, 0x34,
    0x2e, 0x00, 0x00, 0x00, 0x01, 0x5b, 0x00, 0x30, 0x59, 0x30, 0x13, 0x06,
    0x07, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x02, 0x01, 0x06, 0x08, 0x2a, 0x86,
    0x48, 0xce, 0x3d, 0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04, 0x4e, 0x85,
    0x38, 0x35, 0xc7, 0xc1, 0xa4, 0x89, 0x77, 0xb7, 0xd2, 0x83, 0x3c, 0x9a,
    0xa6, 0xb9, 0x07, 0x0c, 0x59, 0x7b, 0x9d, 0xe2, 0xdd, 0xde, 0x87, 0x1b,
    0xd2, 0x41, 0xa1, 0xed, 0xe5, 0x53, 0x40, 0xd6, 0xfe, 0x5c, 0x91, 0x1e,
    0x94, 0x8d, 0xa0, 0x28, 0xaa, 0x38, 0x66, 0x28, 0x01, 0x40, 0xb7, 0xc7,
    0x8d, 0xf0, 0x1a, 0xb8, 0x62, 0x55, 0xa0, 0xa1, 0x8a, 0xea, 0x02, 0x9a,
    0x0e, 0xe5, 0x47, 0x00, 0x30, 0x45, 0x02, 0x20, 0x4e, 0x26, 0x81, 0x2d,
    0x12, 0x7d, 0xa7, 0xf6, 0xb8, 0x9d, 0x5b, 0xbb, 0xcb, 0x76, 0xc4, 0xa8,
    0x2c, 0xb3, 0xa9, 0xfa, 0x10, 0xc4, 0x82, 0x28, 0x86, 0x08, 0x00, 0x42,
    0xed, 0xc3, 0x94, 0x63, 0x02, 0x21, 0x00, 0xc8, 0x06, 0x32, 0xd1, 0x66,
    0xbc, 0x37, 0x82, 0xad, 0x4e, 0xc2, 0xb9, 0x2f, 0x1c, 0xb2, 0x48, 0x06,
    0xe3, 0x92, 0x16, 0x2a, 0x87, 0xd1, 0xde, 0x63, 0x80, 0x97, 0x3f, 0x10,
    0x90, 0xf7, 0x7a
};

static uint8_t priv_key_bin[] = {
    0x30, 0x77, 0x02, 0x01, 0x01, 0x04, 0x20, 0xef, 0x5f, 0x51, 0xfa, 0x24,
    0xea, 0x49, 0x82, 0x35, 0x93, 0x63, 0x92, 0x96, 0xc6, 0x0f, 0x69, 0x16,
    0x74, 0x20, 0xfa, 0x1a, 0xb4, 0x6c, 0xcf, 0x2b, 0x09, 0x6f, 0x1c, 0x32,
    0x4e, 0x7f, 0x28, 0xa0, 0x0a, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d,
    0x03, 0x01, 0x07, 0xa1, 0x44, 0x03, 0x42, 0x00, 0x04, 0x4e, 0x85, 0x38,
    0x35, 0xc7, 0xc1, 0xa4, 0x89, 0x77, 0xb7, 0xd2, 0x83, 0x3c, 0x9a, 0xa6,
    0xb9, 0x07, 0x0c, 0x59, 0x7b, 0x9d, 0xe2, 0xdd, 0xde, 0x87, 0x1b, 0xd2,
    0x41, 0xa1, 0xed, 0xe5, 0x53, 0x40, 0xd6, 0xfe, 0x5c, 0x91, 0x1e, 0x94,
    0x8d, 0xa0, 0x28, 0xaa, 0x38, 0x66, 0x28, 0x01, 0x40, 0xb7, 0xc7, 0x8d,
    0xf0, 0x1a, 0xb8, 0x62, 0x55, 0xa0, 0xa1, 0x8a, 0xea, 0x02, 0x9a, 0x0e,
    0xe5
};

IOT_VLA_SIG_STATIC(iot_cert, 1);
static ecc_key cert_key;
static ecc_key priv_key;

typedef enum {
    CONNECTED,
    GETTING_CERT,
    CERT_RECEIVED,
    CERT_AND_GREETING_SENT
} CentralState;

static CentralState central_state;
static uint8_t* peer_cert_bin = NULL;
static uint16_t peer_cert_len = 0;
static ecc_key peer_cert_key = {0};
#define MAX_SIGNATURES 5
#define REPLY_BUFFER 32
IOT_VLA_SIG_STATIC(peer_iot_cert, MAX_SIGNATURES); // allocate structure supporting up to MAX_SIGNATURES signatures

/**@brief Function for handling characters received by the Nordic UART Service (NUS).
 *
 * @details This function takes a list of characters of length data_len and prints the characters out on UART.
 */
static void ble_nus_chars_received_uart_print(uint8_t * p_data, uint16_t data_len)
{
    ret_code_t ret_val;
    static int peer_cert_idx = 0;
    uint8_t cert_len[2];
    uint16_t msg_len = sizeof(cert_len);
    char reply_buf[REPLY_BUFFER] = {0};
    int reply_size = sizeof(reply_buf);

    NRF_LOG_DEBUG("Receiving data.");
    NRF_LOG_HEXDUMP_DEBUG(p_data, data_len);
    
    switch (central_state) {
        case CONNECTED:
            // save length of cert and allocate memory for cert
            if (peer_cert_bin) {
                free(peer_cert_bin);
            }
            ASSERT(data_len == sizeof(peer_cert_len));
            peer_cert_len = p_data[0];
            peer_cert_len |= p_data[1] << 8;
            peer_cert_idx = 0;
            peer_cert_bin = (uint8_t*) malloc(peer_cert_len);
            if (peer_cert_bin == NULL) {
                APP_ERROR_HANDLER(NRF_ERROR_STORAGE_FULL);
            }
            central_state = GETTING_CERT;
            break;
        case GETTING_CERT:
            ASSERT(peer_cert_idx + data_len <= peer_cert_len); // make sure that we are receiving data within the original bounds
            memcpy(peer_cert_bin + peer_cert_idx, p_data, data_len);
            peer_cert_idx += data_len;
            if (peer_cert_idx == peer_cert_len) {
                // cert received
                memset(peer_iot_cert.signatures, 0, MAX_SIGNATURES*sizeof(IotSignature)); // zero the signatures so old signatures don't appear in new certificates
                
                ret_val = parse_certificate(&peer_iot_cert, peer_cert_bin, 1);
                if (ret_val <= 0) {
                    APP_ERROR_HANDLER(ret_val);
                }
                
                unsigned int amount_read = 0;
                if (peer_cert_key.dp) { // easy way to check if key needs to be freed (check if there are domain parameters set)
                    wc_ecc_free(&peer_cert_key);
                }
                ret_val = wc_EccPublicKeyDecode(peer_iot_cert.cert_key.key,
                                                &amount_read,
                                                &peer_cert_key,
                                                peer_iot_cert.cert_key.key_len);
                if (ret_val) {
                    APP_ERROR_HANDLER(ret_val);
                }
                
                central_state = CERT_RECEIVED;
            }
            break;
        case CERT_RECEIVED:
            if (strncmp("get_cert", p_data, data_len) == 0) {
                cert_len[0] = sizeof(cert_bin) & 0xFF;
                cert_len[1] = (sizeof(cert_bin) >> 8) & 0xFF;
    
                ret_val = ble_nus_c_string_send(&m_ble_nus_c, cert_len, sizeof(cert_len));
                APP_ERROR_CHECK(ret_val);
    
                for (int idx = 0; idx < sizeof(cert_bin); idx += BLE_NUS_MAX_DATA_LEN) {
                    msg_len = MIN(BLE_NUS_MAX_DATA_LEN, sizeof(cert_bin) - idx);
                    ret_val = ble_nus_c_string_send(&m_ble_nus_c, cert_bin + idx, msg_len);
                    APP_ERROR_CHECK(ret_val);
                }
                    
                // verify certificate signature
                ecc_key signature_key;
                unsigned int amount_read = 0;
                ret_val = wc_EccPublicKeyDecode(peer_iot_cert.signatures[0].pubkey.key,
                                                    &amount_read,
                                                    &signature_key,
                                                    peer_iot_cert.signatures[0].pubkey.key_len);
                if (ret_val) {
                    APP_ERROR_HANDLER(ret_val);
                }
                
                int peer_cert_len_no_signs = get_cert_len_without_signatures(&peer_iot_cert);
                ret_val = wc_SignatureVerify(WC_HASH_TYPE_SHA256,
                                                WC_SIGNATURE_TYPE_ECC,
                                                peer_cert_bin,
                                                peer_cert_len_no_signs,
                                                peer_iot_cert.signatures[0].signature,
                                                peer_iot_cert.signatures[0].signature_len,
                                                &signature_key,
                                                sizeof(signature_key));
                wc_ecc_free(&signature_key); // free key even in case of error
                if (ret_val) {
                    APP_ERROR_HANDLER(ret_val);
                }
                
                // send greeting
                
                char greeting[] = "Hello Bob.";
                uint8_t greeting_buf[16] = {0}; // take care of padding
                memcpy(greeting_buf, greeting, sizeof(greeting));
                uint8_t ciphertext[60] = {0}; // for 16 bytes plaintext this should be 48 bytes
                int cipher_size = sizeof(ciphertext);
                
                ret_val = wc_ecc_encrypt(&priv_key, &peer_cert_key, greeting_buf, sizeof(greeting_buf), ciphertext, &cipher_size, NULL);
                if (ret_val != 0) {
                    APP_ERROR_HANDLER(ret_val);
                }
                
                ret_val = ble_nus_c_string_send(&m_ble_nus_c, ciphertext, cipher_size);
                APP_ERROR_CHECK(ret_val);
            
                central_state = CERT_AND_GREETING_SENT;
            }
            break;
        case CERT_AND_GREETING_SENT:
            // receiving reply to greeting
            ret_val = wc_ecc_decrypt(&priv_key, &peer_cert_key, p_data,
                                        data_len, reply_buf, &reply_size, NULL);
            if (ret_val) {
                APP_ERROR_HANDLER(ret_val);
            }
            
            NRF_LOG_DEBUG("Decoded reply to greeting message!");
            NRF_LOG_HEXDUMP_DEBUG(reply_buf, reply_size);
            
            // disconnect from peripheral and shutdown
            ret_val = sd_ble_gap_disconnect(m_ble_nus_c.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(ret_val);
            bsp_indication_set(BSP_INDICATE_IDLE);
            
            nrf_sdh_disable_request();
            
            break;
    }

    if (ECHOBACK_BLE_UART_DATA)
    {
        // Send data back to the peripheral.
        do
        {
            ret_val = ble_nus_c_string_send(&m_ble_nus_c, p_data, data_len);
            if ((ret_val != NRF_SUCCESS) && (ret_val != NRF_ERROR_BUSY))
            {
                NRF_LOG_ERROR("Failed sending NUS message. Error 0x%x. ", ret_val);
                APP_ERROR_CHECK(ret_val);
            }
        } while (ret_val == NRF_ERROR_BUSY);
    }
}


/**@brief   Function for handling app_uart events.
 *
 * @details This function receives a single character from the app_uart module and appends it to
 *          a string. The string is sent over BLE when the last character received is a
 *          'new line' '\n' (hex 0x0A) or if the string reaches the maximum data length.
 */
void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint16_t index = 0;
    uint32_t ret_val;

    switch (p_event->evt_type)
    {
        /**@snippet [Handling data from UART] */
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
            index++;

            if ((data_array[index - 1] == '\n') || (index >= (m_ble_nus_max_data_len)))
            {
                NRF_LOG_DEBUG("Ready to send data over BLE NUS");
                NRF_LOG_HEXDUMP_DEBUG(data_array, index);

                do
                {
                    ret_val = ble_nus_c_string_send(&m_ble_nus_c, data_array, index);
                    if ( (ret_val != NRF_ERROR_INVALID_STATE) && (ret_val != NRF_ERROR_RESOURCES) )
                    {
                        APP_ERROR_CHECK(ret_val);
                    }
                } while (ret_val == NRF_ERROR_RESOURCES);

                index = 0;
            }
            break;

        /**@snippet [Handling data from UART] */
        case APP_UART_COMMUNICATION_ERROR:
            NRF_LOG_ERROR("Communication error occurred while handling UART.");
            APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            NRF_LOG_ERROR("Error occurred in FIFO module used by UART.");
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}


/**@brief Callback handling Nordic UART Service (NUS) client events.
 *
 * @details This function is called to notify the application of NUS client events.
 *
 * @param[in]   p_ble_nus_c   NUS client handle. This identifies the NUS client.
 * @param[in]   p_ble_nus_evt Pointer to the NUS client event.
 */

/**@snippet [Handling events from the ble_nus_c module] */
static void ble_nus_c_evt_handler(ble_nus_c_t * p_ble_nus_c, ble_nus_c_evt_t const * p_ble_nus_evt)
{
    ret_code_t err_code;

    switch (p_ble_nus_evt->evt_type)
    {
        case BLE_NUS_C_EVT_DISCOVERY_COMPLETE:
            NRF_LOG_INFO("Discovery complete.");
            err_code = ble_nus_c_handles_assign(p_ble_nus_c, p_ble_nus_evt->conn_handle, &p_ble_nus_evt->handles);
            APP_ERROR_CHECK(err_code);

            err_code = ble_nus_c_tx_notif_enable(p_ble_nus_c);
            APP_ERROR_CHECK(err_code);
            NRF_LOG_INFO("Connected to device with Nordic UART Service.");
            
            central_state = CONNECTED;
            char msg[] = "get_cert";
            err_code = ble_nus_c_string_send(&m_ble_nus_c, msg, strlen(msg));
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_NUS_C_EVT_NUS_TX_EVT:
            ble_nus_chars_received_uart_print(p_ble_nus_evt->p_data, p_ble_nus_evt->data_len);
            break;

        case BLE_NUS_C_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected.");
            scan_start();
            break;
    }
}
/**@snippet [Handling events from the ble_nus_c module] */


/**
 * @brief Function for handling shutdown events.
 *
 * @param[in]   event       Shutdown type.
 */
static bool shutdown_handler(nrf_pwr_mgmt_evt_t event)
{
    ret_code_t err_code;

    err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    switch (event)
    {
        case NRF_PWR_MGMT_EVT_PREPARE_WAKEUP:
            // Prepare wakeup buttons.
            err_code = bsp_btn_ble_sleep_mode_prepare();
            APP_ERROR_CHECK(err_code);
            break;

        default:
            break;
    }

    return true;
}

NRF_PWR_MGMT_HANDLER_REGISTER(shutdown_handler, APP_SHUTDOWN_HANDLER_PRIORITY);


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t            err_code;
    ble_gap_evt_t const * p_gap_evt = &p_ble_evt->evt.gap_evt;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            err_code = ble_nus_c_handles_assign(&m_ble_nus_c, p_ble_evt->evt.gap_evt.conn_handle, NULL);
            APP_ERROR_CHECK(err_code);

            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);

            // start discovery of services. The NUS Client waits for a discovery result
            err_code = ble_db_discovery_start(&m_db_disc, p_ble_evt->evt.gap_evt.conn_handle);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_DISCONNECTED:

            NRF_LOG_INFO("Disconnected. conn_handle: 0x%x, reason: 0x%x",
                         p_gap_evt->conn_handle,
                         p_gap_evt->params.disconnected.reason);
            break;

        case BLE_GAP_EVT_TIMEOUT:
            if (p_gap_evt->params.timeout.src == BLE_GAP_TIMEOUT_SRC_CONN)
            {
                NRF_LOG_INFO("Connection Request timed out.");
            }
            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported.
            err_code = sd_ble_gap_sec_params_reply(p_ble_evt->evt.gap_evt.conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:
            // Accepting parameters requested by peer.
            err_code = sd_ble_gap_conn_param_update(p_gap_evt->conn_handle,
                                                    &p_gap_evt->params.conn_param_update_request.conn_params);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;
            
        default:
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for handling events from the GATT library. */
void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
    if (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED)
    {
        NRF_LOG_INFO("ATT MTU exchange completed.");

        m_ble_nus_max_data_len = p_evt->params.att_mtu_effective - OPCODE_LENGTH - HANDLE_LENGTH;
        NRF_LOG_INFO("Ble NUS max data length set to 0x%X(%d)", m_ble_nus_max_data_len, m_ble_nus_max_data_len);
    }
}


/**@brief Function for initializing the GATT library. */
void gatt_init(void)
{
    ret_code_t err_code;

    err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_ble_gatt_att_mtu_central_set(&m_gatt, NRF_SDH_BLE_GATT_MAX_MTU_SIZE);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in] event  Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    ret_code_t err_code;

    switch (event)
    {
        case BSP_EVENT_SLEEP:
            nrf_pwr_mgmt_shutdown(NRF_PWR_MGMT_SHUTDOWN_GOTO_SYSOFF);
            break;

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_ble_nus_c.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        default:
            break;
    }
}

/**@brief Function for initializing the UART. */
static void uart_init(void)
{
    ret_code_t err_code;

    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
        .baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);

    APP_ERROR_CHECK(err_code);
}

/**@brief Function for initializing the Nordic UART Service (NUS) client. */
static void nus_c_init(void)
{
    ret_code_t       err_code;
    ble_nus_c_init_t init;

    init.evt_handler = ble_nus_c_evt_handler;

    err_code = ble_nus_c_init(&m_ble_nus_c, &init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing buttons and leds. */
static void buttons_leds_init(void)
{
    ret_code_t err_code;
    bsp_event_t startup_event;

    err_code = bsp_init(BSP_INIT_LEDS, bsp_event_handler);
    APP_ERROR_CHECK(err_code);

    err_code = bsp_btn_ble_init(NULL, &startup_event);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the timer. */
static void timer_init(void)
{
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the nrf log module. */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/** @brief Function for initializing the database discovery module. */
static void db_discovery_init(void)
{
    ret_code_t err_code = ble_db_discovery_init(db_disc_handler);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details Handles any pending log operations, then sleeps until the next event occurs.
 */
static void idle_state_handle(void)
{
    if (NRF_LOG_PROCESS() == false)
    {
        nrf_pwr_mgmt_run();
    }
}

word32 LowResTimer(void)
{
    #if APP_TIMER_CONFIG_RTC_FREQUENCY == 0
        return app_timer_cnt_get() / (APP_TIMER_CLOCK_FREQ / 1);
    #elif APP_TIMER_CONFIG_RTC_FREQUENCY == 1
        return app_timer_cnt_get() / (APP_TIMER_CLOCK_FREQ / 2);
    #elif APP_TIMER_CONFIG_RTC_FREQUENCY == 3
        return app_timer_cnt_get() / (APP_TIMER_CLOCK_FREQ / 3);
    #elif APP_TIMER_CONFIG_RTC_FREQUENCY == 7
        return app_timer_cnt_get() / (APP_TIMER_CLOCK_FREQ / 4);
    #elif APP_TIMER_CONFIG_RTC_FREQUENCY == 15
        return app_timer_cnt_get() / (APP_TIMER_CLOCK_FREQ / 5);
    #elif APP_TIMER_CONFIG_RTC_FREQUENCY == 31
        return app_timer_cnt_get() / (APP_TIMER_CLOCK_FREQ / 6);
    #endif
}

int main(void)
{
    int err = 0;
    
    // Initialize.
    log_init();
    timer_init();
    uart_init();
    buttons_leds_init();
    db_discovery_init();
    power_management_init();
    ble_stack_init();
    gatt_init();
    nus_c_init();
    scan_init();
    
    // Parse certificate
    unsigned int amount_read = 0;
    
    err = parse_certificate(&iot_cert, cert_bin, 1);
    if (err != 1) {
        APP_ERROR_HANDLER(err);
    }
    
    err = wc_EccPublicKeyDecode(iot_cert.cert_key.key,
                                &amount_read,
                                &cert_key,
                                iot_cert.cert_key.key_len);
    if (err != 0) {
        APP_ERROR_HANDLER(err);
    }
    
    amount_read = 0;
    err = wc_EccPrivateKeyDecode(priv_key_bin,
                                    &amount_read,
                                    &priv_key,
                                    sizeof(priv_key_bin));
    if (err != 0) {
        APP_ERROR_HANDLER(err);
    }

    // Start execution.
    NRF_LOG_INFO("Certificate and key parsed.");
    NRF_LOG_INFO("UART started.");
    NRF_LOG_INFO("Debug logging for UART over RTT started.");
    NRF_LOG_FLUSH();
    scan_start();

    // Enter main loop.
    for (;;)
    {
        idle_state_handle();
    }
}
